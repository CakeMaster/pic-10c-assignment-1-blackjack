// I made a new branch called experimental and this comment is the only new thing
// about it so far.
// Another comment

#ifndef Blackjack
#define Blackjack

#include <iostream>
#include <vector>
using namespace std;

const int NUMBER_OF_CARDS_PER_SUIT = 13;
const int NUMBER_OF_SUITS = 4;
const int MAX_NUMBER_BEFORE_BUST = 21;

enum Person { Dealer = 0, Player = 1 };

const int NUMBER_OF_CARDS = NUMBER_OF_CARDS_PER_SUIT * NUMBER_OF_SUITS;
class BlackjackManager
{
private: /* For regular blackjack (example purposes only, 13 cards per suit, 4 suits)
	* 0 - 12 clubs
	* 13 - 25 diamonds
	* 26 - 38 hearts
	* 38 - 51 spades
	* true represents still in deck, false otherwise
	*/
	bool deck[NUMBER_OF_CARDS - 1];
	bool playerBusted;
	bool dealerBusted;
	vector<int> dealerHand;
	vector<int> playerHand;
	int CalculateHandValue(vector<int>* hand);
public: 
	BlackjackManager();
	~BlackjackManager();
	void ResetAll();
	bool DrawCard(Person person);
	Person ReturnWinner();
	int ReturnScore(Person person);
	string DisplayHand(Person person);
	int CalculateHandValue(Person person);

	bool ReturnPlayerBusted() { return playerBusted; }
	bool ReturnDealerBusted() { return dealerBusted; }
};

#endif // !Blackjack.h