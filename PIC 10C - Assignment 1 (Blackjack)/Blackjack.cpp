#include <iostream>
#include "Blackjack.h"
#include <stdlib.h>     /* srand, rand */
#include <ctime>
#include <string>
using namespace std;


BlackjackManager::BlackjackManager()
{
	for (int i = 0; i < NUMBER_OF_CARDS - 1; i++)
		deck[i] = true;

	playerBusted = false;
	dealerBusted = false;

	srand(time(NULL));
}

BlackjackManager::~BlackjackManager()
{
}

void BlackjackManager::ResetAll()
{
	for (int i = 0; i < NUMBER_OF_CARDS - 1; i++)
		deck[i] = true;

	dealerHand.clear();
	playerHand.clear();
	playerBusted = false;
	dealerBusted = false;
}

int BlackjackManager::CalculateHandValue(Person person)
{
	if (person == Player)
		return CalculateHandValue(&playerHand);
	else if (person == Dealer)
		return CalculateHandValue(&dealerHand);
	else
		throw new exception("Invalid Person Enum");
}

int BlackjackManager::CalculateHandValue(vector<int>* hand)
{
	int numberOfAces = 0;
	int handValue = 0;

	for (int i = 0; i < hand->size(); i++)
	{
		int cardValue = (*hand)[i] % NUMBER_OF_CARDS_PER_SUIT + 1;

		if (cardValue == 1)
			numberOfAces++;

		handValue += (cardValue < 10) ? cardValue : 10;
	}

	if (handValue > MAX_NUMBER_BEFORE_BUST)
		return 0;

	while (numberOfAces > 0)
	{
		if (handValue + 10 <= MAX_NUMBER_BEFORE_BUST)
		{
			handValue += 10;
			numberOfAces--;
		}
		else
			break;
	}

	return handValue;
}

bool BlackjackManager::DrawCard(Person person)
{
	bool* bust;
	vector<int>* hand;

	if (person == Player)
	{
		if (playerBusted)
			return false;
		hand = &playerHand;
		bust = &playerBusted;
	}
	else if (person == Dealer)
	{
		if (dealerBusted)
			return false;
		hand = &dealerHand;
		bust = &dealerBusted;
	}
	else
	{
		return false;    // not possible
	}

	int randomCard = rand() % NUMBER_OF_CARDS;
	int counter = 0;
	while (deck[randomCard] == false)
	{
		randomCard = rand() % NUMBER_OF_CARDS;

		if (counter >= 100)
		{
			for (int i = 0; i < NUMBER_OF_CARDS; i++)
				if (deck[i] == true)
				{
					counter = 0;
					continue;
				}
			throw new exception("Trying to draw card; however, no more cards in deck!");
		}
		counter++;
	}

	deck[randomCard] == false;
	hand->push_back(randomCard);

	if(CalculateHandValue(hand) == 0)
		*bust = true;

	return true;
}

Person BlackjackManager::ReturnWinner()
{
	if (playerBusted)
		return Dealer;
	if (dealerBusted)
		return Player;

	return (CalculateHandValue(&playerHand) > CalculateHandValue(&dealerHand)) ? Player : Dealer;
}

int BlackjackManager::ReturnScore(Person person)
{
	vector<int>* hand = nullptr;

	if (person == Player)
	{
		if (playerBusted)
			return 0;
		hand = &playerHand;
	}
	else if (person == Dealer)
	{
		if (dealerBusted)
			return 0;
		hand = &dealerHand;
	}

	int score = CalculateHandValue(hand);

	return score;
}

string CardNumberToString(int num)
{
	int value = num % NUMBER_OF_CARDS_PER_SUIT + 1;
	int suit = num % NUMBER_OF_SUITS;

	string returnValue = "";
	switch (value)
	{
	case 11:
		returnValue = "J";
		break;
	case 12:
		returnValue = "Q";
		break;
	case 13:
		returnValue = "K";
		break;
	case 1:
		returnValue = "A";
		break;
	default:
		returnValue = to_string(value);
		break;
	}

	string returnSuit = "";
	switch (suit)
	{
	case 0:
		returnSuit = "C";
		break;
	case 1:
		returnSuit = "D";
		break;
	case 2:
		returnSuit = "H";
		break;
	case 3:
		returnSuit = "S";
		break;
	}

	return returnValue + returnSuit;
}



string BlackjackManager::DisplayHand(Person person)
{
	string info = "";
	vector<int>* hand = nullptr;
	if (person == Player)
	{
		info += "You have: ";
		hand = &playerHand;
	}
	else if (person == Dealer)
	{
		info += "The dealer has: ";
		hand = &dealerHand;
	}

	if (hand->size() > 0) { info += CardNumberToString((*hand)[0]); }
	for (int i = 1; i < hand->size(); i++)
	{
		info += ", " + CardNumberToString((*hand)[i]);
	}

	return info;
}

bool AskToContinue()
{
	string inputLine = "";

	cout << "Would you like to play again? (y/n)" << endl;

	while (true)
	{
		getline(cin, inputLine);

		if (inputLine == "Y" || inputLine == "y")
		{
			return true;
			break;
		}
		else if (inputLine == "N" || inputLine == "n")
		{
			return false;
			break;
		}
		else
		{
			cout << "Invalid entry, please enter 'y' or 'n'." << endl;
		}
	}
}

int main()
{
	BlackjackManager* BJM = new BlackjackManager();
	string inputLine = "";

	cout << "Welcome to blackjack. This game is unfair and completely stacked against you." 
		 << "The dealer wins all ties and plays after you. If you bust, the dealer automatically wins." 
		 << endl;

	do {BJM->ResetAll();

		BJM->DrawCard(Player);
		BJM->DrawCard(Dealer);
		BJM->DrawCard(Player);
		BJM->DrawCard(Dealer);

		cout << BJM->DisplayHand(Player) << endl;

		while (!BJM->ReturnPlayerBusted())
		{
			cout << "Your score: " << to_string(BJM->CalculateHandValue(Player)) << endl 
				 << "Would you like to draw? (y/n)" << endl;
			getline(cin, inputLine);

			if (inputLine == "Y" || inputLine == "y")
			{
				BJM->DrawCard(Player);
				cout << BJM->DisplayHand(Player) << endl;
			}
			else if (inputLine == "N" || inputLine == "n")
				break;
			else
				cout << "Invalid entry, please enter 'y' or 'n'." << endl;
		}

		if (BJM->ReturnPlayerBusted())
		{
			cout << "You bust! The dealer has won this round." << endl; // dealer wins
			getline(cin, inputLine);
			continue;
		}


		cout << BJM->DisplayHand(Dealer) << endl;

		while (BJM->ReturnScore(Dealer) < BJM->ReturnScore(Player))
		{
			cout << "Dealer draws a card." << endl;
			BJM->DrawCard(Dealer);
			cout << BJM->DisplayHand(Dealer) << endl;

			if (BJM->ReturnDealerBusted())
			{
				cout << "Dealer bust! You have won! Congratulations!" << endl;  // player wins
				getline(cin, inputLine);
				break;
			}
		}

		if (BJM->ReturnDealerBusted())
			continue;

		cout << "Your Score: " << to_string(BJM->CalculateHandValue(Player)) << ",  "
			<< "Dealer's Score: " << to_string(BJM->CalculateHandValue(Dealer)) << "   "
			<< "The dealer has won." << endl;

		getline(cin, inputLine);

	} while (AskToContinue());

	return 0;
}